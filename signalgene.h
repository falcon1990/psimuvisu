#ifndef GENERATORALEATOR_H
#define GENERATORALEATOR_H

#include "QtCore"

#define Nb_type 7

class SignalGene
{

public:
    static void init(int nbSign = 1);
    static float getNumber(int iSignal);
    static void setMax(float max);
    static void setMin(float min);
    static void setSignal(QString s);
    static QStringList getSignaList();
    static float getMax();
    static float getMin();
    static int getIndexData();

private:
    SignalGene() {}   // Disallow creating an instance of this object
    static float max;
    static float min;
    static int signType;
    static QList<int> cptSign[Nb_type];
    static QStringList qlsSignal;
};

#endif // GENERATORALEATOR_H
