#ifndef CLIENT_H
#define CLIENT_H

#include "QtCore"
#include <QMainWindow>
#include "QtNetwork/QTcpSocket"

class Client : public QObject
{
    Q_OBJECT

public:
    Client(QObject *parent);
    virtual ~Client();
    void getVersion();

public slots:
    void readTcpData();

private:
    QTcpSocket* socket;
    QByteArray data;
};

#endif // CLIENT_H
