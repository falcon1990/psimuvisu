#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCharts>
#include <QResizeEvent>
#include "tickdata.h"
#include "chart.h"
#include <QCloseEvent>
#include "Client.h"

#define Nb_Signal 5

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void resizeEvent(QResizeEvent* event);
    void showEvent(QShowEvent* event);
    bool eventFilter(QObject *watched, QEvent *event);
    void closeEvent (QCloseEvent *event);
    void callBack_newData();
    void infoLine(QPointF point, bool state);

private slots:
    void on_bStart_clicked();
    void on_bStop_clicked();
    void on_dVitesse_valueChanged(int value);
    void on_sZoom_valueChanged(int value);
    void on_bCursor_clicked();
    void on_bResetZoom_clicked();
    void on_sPosition_valueChanged(int value);
    void on_cbSignType_currentTextChanged(const QString &arg1);
    void on_cbSignNumber_currentTextChanged(const QString &arg1);
    void on_bSignSet_clicked();
    void on_bDataBase_clicked();
    void on_bVersion_clicked();

public slots:
    void print(bool b);
    void rubberZoomAdapt(QEvent* event);
    void infoCursor(QEvent* event);
    void comDataBaseHandle();

signals:
    void rubberBandChanged(QEvent* event);
    void infoCursorAsked(QEvent* event);

private:
    void updateCursor(bool visible, QMouseEvent* mouseEvent);

    Ui::MainWindow *ui;
    QWidget* qwDataBase;

    QChartView* qcvChart;
    Chart* chart;
    QRubberBand* qRubberBand;
    QGraphicsLineItem* qgliLineX;
    QGraphicsLineItem* qgliLineY;
    QGraphicsTextItem* qgtiCoorX;
    QGraphicsTextItem* qgtiCoorY;
    QGraphicsLineItem* qgliLineCoorY[Nb_Signal];
    QGraphicsLineItem* qgliLineCoorX;
    QGraphicsTextItem* qgtiTextCoorY[Nb_Signal];
    QGraphicsTextItem* qgtiTextCoorX;
    QPlainTextEdit* qpteComDataBase;

    TickData* td1;

    int index;
    bool bCursorState;
    QRectF zoomAera;
    float scaledZoom;
    float positionZoom;
    QString signType;
    int signNumber;
    QString sign[Nb_Signal];

    Client* client;
};

#endif // MAINWINDOW_H
