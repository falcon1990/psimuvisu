#include "signalgene.h"
#include "time.h"
#include <stdio.h>
#include <stdlib.h>
#include "QtCore"
#include "QTime"
#include "QtMath"


float SignalGene::max = 100;
float SignalGene::min = -100;
int SignalGene::signType = 0;
QList<int> SignalGene::cptSign[];
QStringList SignalGene::qlsSignal;

void SignalGene::init(int nbSign)
{
    int iType = 0;
    int iSign = 0;
    for(iType = 0; iType < Nb_type; iType++)
        for(iSign = 0; iSign < nbSign; iSign++)
            SignalGene::cptSign[iType].append(0);
    qsrand(QTime::currentTime().msec());
    SignalGene::qlsSignal.append("Ale");
    SignalGene::qlsSignal.append("Sin");
    SignalGene::qlsSignal.append("Cos");
    SignalGene::qlsSignal.append("DSin");
    SignalGene::qlsSignal.append("Sin+DSin");
    SignalGene::qlsSignal.append("DSin+DSin");
    SignalGene::qlsSignal.append("Tri");
}

float SignalGene::getNumber(int iSignal)
{
    float result = 0;
    switch(SignalGene::signType)
    {
        case 0:
        {
            float a = SignalGene::min;
            float b = SignalGene::max;
            result = ( qrand()/(double)RAND_MAX ) * (b-a) + a;
            break;
        }

        case 1:
        {
            int sinVal = SignalGene::cptSign[SignalGene::signType].at(iSignal);
            result = (float)(SignalGene::max-1)*qSin((float)(((2*M_PI)/50.0)*sinVal));
            sinVal = (++sinVal >= 50) ? 0 : sinVal;
            SignalGene::cptSign[SignalGene::signType].replace(iSignal, sinVal);
            break;
        }

        case 2:
        {
            int cosVal = SignalGene::cptSign[SignalGene::signType].at(iSignal);
            result = (float)(SignalGene::max-1)*qCos((float)(((2*M_PI)/50.0)*cosVal));
            cosVal = (++cosVal >= 50) ? 0 : cosVal;
            SignalGene::cptSign[SignalGene::signType].replace(iSignal, cosVal);
            break;
        }

        case 3:
        {
            int dsinVal = SignalGene::cptSign[SignalGene::signType].at(iSignal);
            result = (float)qFabs((float)(SignalGene::max-1)*qSin((float)(((2*M_PI)/50.0)*dsinVal)));
            dsinVal = (++dsinVal >= 50) ? 0 : dsinVal;
            SignalGene::cptSign[SignalGene::signType].replace(iSignal, dsinVal);
            break;
        }

        case 4:
        {
            int sinDSin = SignalGene::cptSign[SignalGene::signType].at(iSignal);
            float r1 = (float)(SignalGene::max-1)*qSin((float)(((2*M_PI)/50.0)*sinDSin));
            float r2 = (float)qFabs((float)(SignalGene::max-1)*qSin((float)(((2*M_PI)/50.0)*sinDSin)));
            result = (float)(((float)(r1)+(float)(r2))-(SignalGene::max-1));
            sinDSin = (++sinDSin >= 50) ? 0 : sinDSin;
            SignalGene::cptSign[SignalGene::signType].replace(iSignal, sinDSin);
            break;
        }

        case 5:
        {
            int dSinDSin = SignalGene::cptSign[SignalGene::signType].at(iSignal);
            float r1 = (float)(SignalGene::max-1)*qSin((float)(((2*M_PI)/50.0)*dSinDSin));
            float r2 = (float)qFabs((float)(SignalGene::max-1)*qSin((float)(((2*M_PI)/50.0)*dSinDSin)));
            float r3 = (float)(SignalGene::max-1)*qSin((float)(((2*M_PI)/50.0)*dSinDSin-(M_PI)));
            float r4 = (float)qFabs((float)(SignalGene::max-1)*qSin((float)(((2*M_PI)/50.0)*dSinDSin-(M_PI))));
            float rr1 = (float)(((float)(r1)+(float)(r2)));
            float rr2 = (float)(((float)(r3)+(float)(r4)));
            result = (float)(((float)rr1 + (float)rr2)-(SignalGene::max-1));
            dSinDSin = (++dSinDSin >= 50) ? 0 : dSinDSin;
            SignalGene::cptSign[SignalGene::signType].replace(iSignal, dSinDSin);
            break;
        }

        case 6:
        {
            int triState = SignalGene::cptSign[SignalGene::signType].at(iSignal);
            if(triState == false)
                result = (float)SignalGene::max - 1;
            else
                result = (float)SignalGene::min + 1;
            triState = (~triState) & 0x1;
            SignalGene::cptSign[SignalGene::signType].replace(iSignal, triState);
            break;
        }

        default:
        {
            SignalGene::signType = 0;
            break;
        }
    }
    return result;
}

void SignalGene::setMax(float max)
{
    SignalGene::max = max;
}

void SignalGene::setMin(float min)
{
    SignalGene::min = min;
}

void SignalGene::setSignal(QString s)
{
    SignalGene::signType = SignalGene::qlsSignal.indexOf(s);
}

QStringList SignalGene::getSignaList()
{
    return SignalGene::qlsSignal;
}

float SignalGene::getMax()
{
    return SignalGene::max;
}

float SignalGene::getMin()
{
    return SignalGene::min;
}

int SignalGene::getIndexData()
{
    int maxElem = 0;
    int oldmaxElem = 0;
    int i = 0;
    for(i = 0; i < Nb_type; i++)
    {
        maxElem = *std::max_element(SignalGene::cptSign[i].begin(), SignalGene::cptSign[i].end());
        if(oldmaxElem > maxElem)
            maxElem = oldmaxElem;
    }
    return maxElem;
}
