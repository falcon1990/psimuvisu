#ifndef CHART_H
#define CHART_H

#include <QtCharts>
#include <QtCore>
#include "QQueue"

#define Nb_Signal 5

class Chart: public QChart
{
    Q_OBJECT

public:
    Chart(QGraphicsItem *parent = 0, Qt::WindowFlags wFlags = 0);
    virtual ~Chart();
    void showEvent(QShowEvent* event);
    void addValue(float* val);
    void setZoom(float scaledZoom);
    void resetZoom();
    void zoomInRubberBand(float min, float max);
    void setPosition(float positionZoom);
    qreal getMinAxisX();
    qreal getMaxAxisX();
    qreal getMinAxisY();
    qreal getMaxAxisY();
public slots:
    void handleTimeout();

private:
    void refreshAxis();
    float newValueArea(float lastValue, int iSerie);

    QTimer qtimer;
    QLineSeries* qlsSeries[Nb_Signal];
    QLineSeries* qlsMoySeries[Nb_Signal];
    QAreaSeries* qasSeries[Nb_Signal];
    QValueAxis* vaAxisX;
    QValueAxis* vaAxisY;
    QQueue<float>* qQueue[Nb_Signal];
    qreal xValue;
    qreal yValue;
    float zoom;
    float positionZoom;
    bool enaRubberBand;
    float xRubberBandMin;
    float xRubberBandMax;
    QList<float> lMoy[Nb_Signal];
};

#endif /* CHART_H */
