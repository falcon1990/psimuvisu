#include "chart.h"
#include <QtCharts>
#include <QtCore>
#include "QQueue"
#include "signalgene.h"

#define AREA 0          //1:enable; 0:desable
#define N_MOYGLISS 5    //size QList for mean

//Utilitaire
//    this->yValue = QRandomGenerator::global()->bounded(5) - 2.5;


Chart::Chart(QGraphicsItem *parent, Qt::WindowFlags wFlags):
    QChart(QChart::ChartTypeCartesian, parent, wFlags),
    vaAxisX(nullptr),
    vaAxisY(nullptr),
    xValue(0),
    yValue(0)
{
    //Initialisation
    this->zoom = 0;
    this->positionZoom = 0.2;
    this->enaRubberBand = false;
    this->xRubberBandMin = 0;
    this->xRubberBandMax = 0;

    int iSerie = 0;
    for(iSerie = 0; iSerie < Nb_Signal; iSerie++)
    {
        this->qQueue[iSerie] = new QQueue<float>;
        this->qlsSeries[iSerie] = new QLineSeries(this);
#if AREA == 1
        this->qlsMoySeries[iSerie] = new QLineSeries(this);
        this->qasSeries[iSerie] = new QAreaSeries(this->qlsSeries[iSerie], this->qlsMoySeries[iSerie]);
#endif
    }

    //Geometrie
    for(iSerie = 0; iSerie < Nb_Signal; iSerie++)
        this->addSeries(this->qlsSeries[iSerie]);
#if AREA == 1
    for(iSerie = 0; iSerie < Nb_Signal; iSerie++)
        this->addSeries(this->qasSeries[iSerie]);
#endif
    this->createDefaultAxes();
    this->vaAxisX = qobject_cast<QValueAxis*>(this->axisX());
    this->vaAxisY = qobject_cast<QValueAxis*>(this->axisY());
    this->vaAxisX->setRange(0, 10);
    this->vaAxisY->setRange(-100, 100);
    this->vaAxisX->setTickCount(20);
    this->vaAxisY->setTickCount(11);
    this->vaAxisY->setMinorTickCount(4);
#if AREA == 1
    for(iSerie = 0; iSerie < Nb_Signal; iSerie++)
    {
        this->qasSeries[iSerie]->attachAxis(this->vaAxisX);
        this->qasSeries[iSerie]->attachAxis(this->vaAxisY);
    }
#endif

    //Apparence
    this->vaAxisX->setGridLinePen(QPen(QBrush(QColor(160, 160, 160, 255),Qt::SolidPattern),
                                       (qreal)0.5, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));
    this->vaAxisY->setGridLinePen(QPen(QBrush(QColor(160, 160, 160, 255),Qt::SolidPattern),
                                       (qreal)0.5, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));
    this->vaAxisX->setMinorGridLinePen(QPen(QBrush(QColor(220, 220, 220, 255),Qt::SolidPattern),
                                       (qreal)0.5, Qt::DashLine, Qt::SquareCap, Qt::MiterJoin));
    this->vaAxisY->setMinorGridLinePen(QPen(QBrush(QColor(220, 220, 220, 255),Qt::SolidPattern),
                                       (qreal)0.5, Qt::DashLine, Qt::SquareCap, Qt::MiterJoin));
    this->vaAxisX->setLinePen(QPen(QBrush(QColor(0,0,0,255),Qt::SolidPattern),
                              (qreal)0.5, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));
    this->vaAxisY->setLinePen(QPen(QBrush(QColor(0,0,0,255),Qt::SolidPattern),
                              (qreal)0.5, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));
    this->vaAxisX->setGridLineVisible(true);
    this->vaAxisY->setGridLineVisible(true);
    QFont fontAxis = this->vaAxisX->labelsFont();
    fontAxis.setPixelSize(5);
    this->vaAxisX->setLabelsFont(fontAxis);
    this->vaAxisY->setLabelsFont(fontAxis);

    for(iSerie = 0; iSerie < Nb_Signal; iSerie++)
    {
        this->qlsSeries[iSerie]->setPen(QPen(QBrush(this->qlsSeries[iSerie]->pen().color(),Qt::SolidPattern),
                                 (qreal)0.5, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));
        this->qlsSeries[iSerie]->setUseOpenGL(false);
#if AREA == 1
        QColor qcAsS = QColor(this->qlsSeries[iSerie]->color());
        qcAsS.setAlpha(50);
        this->qasSeries[iSerie]->setPen(QPen(QBrush(QColor(0,0,0,0),Qt::SolidPattern),
                                 (qreal)0.5, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));
        this->qasSeries[iSerie]->setColor(qcAsS);
#endif
    }

//    this->setTitle("Dynamic chart");
    this->legend()->hide();
    this->setAnimationOptions(QChart::NoAnimation);
    this->setMargins(QMargins(0,0,0,0));
    this->setContentsMargins(0, 0, 0, 0);
    this->setBackgroundRoundness(0);

    //Connexion
    QObject::connect(&this->qtimer, &QTimer::timeout, this, &Chart::handleTimeout);

    //Starting
    this->qtimer.setInterval(200);
    this->qtimer.start();
}

Chart::~Chart()
{
    foreach(QAbstractSeries* serie, this->series())
    {
        this->removeSeries(serie);
        delete serie;
    }

    int i = 0;
    for(i = 0; i < Nb_Signal; i++)
        delete this->qQueue[i];
}

void Chart::addValue(float* val)
{
    int iSerie = 0;
    float max = SignalGene::getMax()-1;
    float min = SignalGene::getMin()+1;
    for(iSerie = 0; iSerie < Nb_Signal; iSerie++)
    {
        val[iSerie] = (val[iSerie]/Nb_Signal)+(min+((iSerie*2+1)*((max-min)/(2.0*Nb_Signal))));
        this->qQueue[iSerie]->enqueue(val[iSerie]);
    }
}

void Chart::setZoom(float scaledZoom)
{
    this->zoom = scaledZoom;
    this->refreshAxis();
}

void Chart::resetZoom()
{
    this->vaAxisX->setRange(0, this->xValue);
    this->enaRubberBand = false;
}

void Chart::zoomInRubberBand(float min, float max)
{
    this->xRubberBandMax = max;
    this->xRubberBandMin = min;
    this->enaRubberBand = true;
}

void Chart::setPosition(float positionZoom)
{
    this->positionZoom = positionZoom;
    this->refreshAxis();
}

qreal Chart::getMinAxisX()
{
    return this->vaAxisX->min();
}

qreal Chart::getMaxAxisX()
{
    return this->vaAxisX->max();
}

qreal Chart::getMinAxisY()
{
    return this->vaAxisY->min();
}

qreal Chart::getMaxAxisY()
{
    return this->vaAxisY->max();
}

void Chart::handleTimeout()
{
    float lastValue = 0;
    qreal xValue = 0;
    int iSerie = 0;
    for(iSerie = 0; iSerie < Nb_Signal; iSerie++)
    {
        xValue = this->xValue;
        while(this->qQueue[iSerie]->isEmpty() == false)
        {
            lastValue = this->qQueue[iSerie]->dequeue();
            this->qlsSeries[iSerie]->append(xValue, lastValue);
#if AREA == 1
            this->qlsMoySeries[iSerie]->append(xValue, this->newValueArea(lastValue, iSerie));
#endif
            xValue++;
        }
    }
    this->xValue = xValue;
    this->refreshAxis();
}

void Chart::showEvent(QShowEvent* event)
{
    QChart::showEvent(event);
}

void Chart::refreshAxis()
{
    if(this->xValue != 0)    //Avoid that the X axis doesn't appears
    {
        if(this->enaRubberBand == false)
        {
            float rangeMin = this->xValue*this->zoom;
            float rangeMax = this->xValue;
            float deltaRange = rangeMax-rangeMin;
            rangeMin = rangeMin+(deltaRange*this->positionZoom);
            rangeMin = (this->zoom <= 0) ? (0) : (rangeMin);
            rangeMax = rangeMax+(deltaRange*this->positionZoom);
            this->vaAxisX->setRange(rangeMin, rangeMax);
        }
        else
        {
            this->vaAxisX->setRange(this->xRubberBandMin, this->xRubberBandMax);
        }
    }
}

float Chart::newValueArea(float lastValue, int iSerie)
{
    static float moy = 0;
    if(this->lMoy[iSerie].length() < N_MOYGLISS)
    {
        this->lMoy[iSerie].append(lastValue);
        moy = 0;
        QList<float>::iterator it;
        for(it = this->lMoy[iSerie].begin(); it != this->lMoy[iSerie].end(); it++)
            moy += *it;
        moy /= this->lMoy[iSerie].length();
        if(this->lMoy[iSerie].length() == N_MOYGLISS)
            this->lMoy[iSerie].removeFirst();
    }
    return moy;
}

