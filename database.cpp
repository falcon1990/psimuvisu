#include "DataBase.h"
#include <stdio.h>
#include <stdlib.h>
#include "QtCore"
#include "QTime"
#include "QtMath"
#include "QtSql"

const QString DataBase::creatTableChantier =
      QString("CREATE TABLE IF NOT EXISTS Chantier("
              "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
              "debut DATETIME NOT NULL,"
              "fin DATETIME NOT NULL,"
              "nom VARCHAR(30) NOT NULL,"
              "numero BIGINT UNSIGNED NOT NULL"
              ");");

const QString DataBase::creatTableMachine =
      QString("CREATE TABLE IF NOT EXISTS Machine("
              "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
              "debut DATETIME NOT NULL,"
              "fin DATETIME NOT NULL,"
              "nom VARCHAR(30) NOT NULL,"
              "numero BIGINT UNSIGNED NOT NULL,"
              "chantierId BIGINT UNSIGNED NOT NULL,"
              "FOREIGN KEY(chantierId) REFERENCES Chantier(id)"
              ");");

const QString DataBase::creatTableMesure =
      QString("CREATE TABLE IF NOT EXISTS Mesure("
              "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
              "temps DATETIME NOT NULL,"
              "pk BIGINT UNSIGNED DEFAULT NULL,"
              "ecartement FLOAT DEFAULT NULL,"
              "trace FLOAT DEFAULT NULL,"
              "devers FLOAT DEFAULT NULL,"
              "gauche FLOAT DEFAULT NULL,"
              "profil FLOAT DEFAULT NULL,"
              "machineId BIGINT UNSIGNED NOT NULL,"
              "chantierId BIGINT UNSIGNED NOT NULL,"
              "FOREIGN KEY(machineId) REFERENCES Machine(id),"
              "FOREIGN KEY(chantierId) REFERENCES Chantier(id)"
              ");");

const QString DataBase::createIndexChantier =
      QString("CREATE UNIQUE INDEX "
              "induniChantierDebutFin "
              "ON Chantier(debut, fin);");

const QString DataBase::createIndexMachine =
      QString("CREATE UNIQUE INDEX "
              "induniMachineDebutFinChantierid "
              "ON  Machine(debut, fin, chantierId);");

const QString DataBase::createIndexMesure =
      QString("CREATE UNIQUE INDEX "
              "induniMesureTempsPkMachineidChantierid "
              "ON Mesure(temps, pk, machineId, chantierId);");

const QString DataBase::createDataChantier =
      QString("INSERT INTO Chantier(debut, fin, nom, numero)"
              "VALUES(DATETIME('now'), DATETIME('now', '+6 month'), 'OrientExpress', 201804);");

const QString DataBase::createDataMachine =
      QString("INSERT INTO Machine(debut, fin, nom, numero, chantierId)"
              "VALUES(DATETIME('now'), DATETIME('now', '+3 hours'), 'DemiLune', 2001, 1),"
              "(DATETIME('now', '+3 hours'), DATETIME('now', '+3 hours'), 'TroimiLune', 3001, 1);");

const QString DataBase::createDataMesure =
      QString("INSERT INTO Mesure(temps, pk, ecartement, trace, devers, gauche, profil, machineId, chantierId)"
              "VALUES(DATETIME('now'), 10000, 1435.0, 5, 5, 1, 10, 1, 1),"
              "(DATETIME('now'), 10001, 1439.5, 10, 25, 2.2, 11, 1, 1),"
              "(DATETIME('now'), 10002, 1436.2, 15, 35, 1.5, 12, 1, 1),"
              "(DATETIME('now'), 10003, 1434.3, 20, 45, 3.9, 13, 1, 1),"
              "(DATETIME('now'), 10004, 1432.8, 22, 55, 4.7, 14, 1, 1),"
              "(DATETIME('now'), 10005, 1435.3, 18, 65, 2.5, 15, 1, 1),"
              "(DATETIME('now'), 10006, 1434.5, 12, 75, 2.3, 20, 1, 1),"
              "(DATETIME('now'), 10007, 1431.4, 9, 65, 4.4, 25, 1, 1),"
              "(DATETIME('now'), 10008, 1432.6, 7, 55, 3.9, 28, 1, 1),"
              "(DATETIME('now'), 10009, 1435.0, 5, 45, 1.8, 16, 1, 1),"
              "(DATETIME('now'), 20000, 1433.3, 5, 35, 2.4, 5, 2, 1),"
              "(DATETIME('now'), 20001, 1434.4, 6, 65, 1.6, 10, 2, 1),"
              "(DATETIME('now'), 20002, 1435.8, 7, 95, 2.6, 20, 2, 1),"
              "(DATETIME('now'), 20003, 1437.4, 8, 75, 3.4, 30, 2, 1),"
              "(DATETIME('now'), 20004, 1436.1, 9, 55, 4.8, 28, 2, 1),"
              "(DATETIME('now'), 20005, 1434.2, 10, 35, 3.9, 27, 2, 1),"
              "(DATETIME('now'), 20006, 1436.2, 9, 15, 2.6, 26, 2, 1),"
              "(DATETIME('now'), 20007, 1436.4, 8, 25, 1.8, 25, 2, 1),"
              "(DATETIME('now'), 20008, 1437.6, 7, 35, 1.2, 24, 2, 1),"
              "(DATETIME('now'), 20009, 1435.1, 6, 45, 2.9, 23, 2, 1);");

QSqlDatabase DataBase::dataBase;
QSqlQuery DataBase::query;

bool DataBase::initDataBase()
{
    DataBase::dataBase = QSqlDatabase::addDatabase("QSQLITE");
    DataBase::dataBase.setDatabaseName(".\\database\\geometrie.db");

    if (DataBase::dataBase.open() == false)
    {
        qDebug() << DataBase::dataBase.lastError();
        return false;
    }

    DataBase::query = QSqlQuery(DataBase::dataBase);

    QStringList tables = DataBase::dataBase.tables();
    if(tables.contains("Chantier", Qt::CaseInsensitive) == false)
    {
        if(DataBase::query.exec(DataBase::creatTableChantier) == false)
        {
            qDebug() << DataBase::dataBase.lastError();
            return false;
        }
        if(DataBase::query.exec(DataBase::createIndexChantier) == false)
        {
            qDebug() << DataBase::dataBase.lastError();
            return false;
        }
        if(DataBase::query.exec(DataBase::createDataChantier) == false)
        {
            qDebug() << DataBase::dataBase.lastError();
            return false;
        }
    }
    if(tables.contains("Machine", Qt::CaseInsensitive) == false)
    {
        if(DataBase::query.exec(DataBase::creatTableMachine) == false)
        {
            qDebug() << DataBase::dataBase.lastError();
            return false;
        }
        if(DataBase::query.exec(DataBase::createIndexMachine) == false)
        {
            qDebug() << DataBase::dataBase.lastError();
            return false;
        }
        if(DataBase::query.exec(DataBase::createDataMachine) == false)
        {
            qDebug() << DataBase::dataBase.lastError();
            return false;
        }
    }
    if(tables.contains("Mesure", Qt::CaseInsensitive) == false)
    {
        if(DataBase::query.exec(DataBase::creatTableMesure) == false)
        {
            qDebug() << DataBase::dataBase.lastError();
            return false;
        }
        if(DataBase::query.exec(DataBase::createIndexMesure) == false)
        {
            qDebug() << DataBase::dataBase.lastError();
            return false;
        }
        if(DataBase::query.exec(DataBase::createDataMesure) == false)
        {
            qDebug() << DataBase::dataBase.lastError();
            return false;
        }
    }

    return true;//QSqlError();
}

bool DataBase::addMesure(float* dataMesure)
{
    QString newMesure = QString("INSERT INTO Mesure(temps, pk, ecartement, trace, devers, gauche, profil, machineId, chantierId)"
                                "VALUES(DATETIME('now'), %1, %2, %3, %4, %5, %6, %7, %8)")
                                .arg(dataMesure[0], 0, 'f', 0)
                                .arg(dataMesure[1], 2, 'f', 2)
                                .arg(dataMesure[2], 2, 'f', 2)
                                .arg(dataMesure[3], 2, 'f', 2)
                                .arg(dataMesure[4], 2, 'f', 2)
                                .arg(dataMesure[5], 2, 'f', 2)
                                .arg(dataMesure[6], 0, 'f', 0)
                                .arg(dataMesure[7], 0, 'f', 0);

    if(DataBase::query.exec(newMesure) == false)
    {
        qDebug() << DataBase::dataBase.lastError();
        return false;
    }
    return true;
}

QList<QString> DataBase::sendCommand(QString comSQL)
{
    QList<QString> answer;
    QString constitute;
    if(DataBase::query.exec(comSQL) == false)
    {
        qDebug() << DataBase::dataBase.lastError();
//        return nullptr;
    }
    while(DataBase::query.next() == true)
    {
        QString data;
        int i = 0;
        while(true) //TODO: Warning, possible bug responsability
        {
            data = DataBase::query.value(i++).toString();
            if(data.isEmpty() == false)
            {
                constitute.append(data);
                constitute.append(" | ");
            }
            else
                break;
        }
        answer.append(constitute);
        constitute.clear();
    }
    return answer;
}
