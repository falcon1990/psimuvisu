#ifndef DATABASE_H
#define DATABASE_H


#include "QtSql"



class DataBase
{

public:
    static bool initDataBase();
    static bool addMesure(float* dataMesure);
    static QList<QString> sendCommand(QString comSQL);

private:
    DataBase() {}
    static QSqlDatabase dataBase;
    static QSqlQuery query;

    static const QString creatTableMesure;
    static const QString creatTableMachine;
    static const QString creatTableChantier;
    static const QString createIndexMesure;
    static const QString createIndexMachine;
    static const QString createIndexChantier;
    static const QString createDataChantier;
    static const QString createDataMachine;
    static const QString createDataMesure;
};

#endif // DATABASE_H
