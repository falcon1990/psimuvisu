#include "client.h"
#include "QtCore"
#include "QMainWindow"
#include "QtNetwork/QTcpSocket"

Client::Client(QObject* parent):
    QObject(parent),
    socket(new QTcpSocket(this))
{
    this->socket->connectToHost("127.0.0.1", 9008);

    QObject::connect(this->socket,
                     &QTcpSocket::readyRead,
                     this,
                     &Client::readTcpData);
}

Client::~Client()
{

}

void Client::readTcpData()
{
    this->data = this->socket->readAll();
    qDebug() << "Client: readTcpData: " << this->data;
}

void Client::getVersion()
{
    this->socket->write(QString("\02V\03").toUtf8());
}
