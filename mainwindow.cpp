#include <cassert> // for assert()
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "signalgene.h"
#include "tickdata.h"
#include "QtCore"
#include "QtGlobal"
#include <QtWidgets>
#include <QtCharts>
#include "QStringBuilder"
#include "QtMath"
#include "chart.h"
#include "QGraphicsView"
#include "QEvent"
#include "QMouseEvent"
#include "QPrinter"
#include "QPrintDialog"
#include "database.h"
#include <QCloseEvent>
#include "Client.h"

#define DATABASE 0

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    chart(new Chart),
    qcvChart(new QChartView),
    ui(new Ui::MainWindow),
    qwDataBase(nullptr),
    qRubberBand(nullptr)
{
    //Initialisation
    ui->setupUi(this);
    this->index = 0;
    this->bCursorState = false;
    this->scaledZoom = 0;
    this->positionZoom = 0.2;
    this->td1 = new TickData();
    int igi = 0;
    SignalGene::init(Nb_Signal);
    DataBase::initDataBase();
    this->client = new Client(this);

    //Geometrie
    ui->dVitesse->setRange(10, 1000);
//    ui->sZoom->setRange(0,99);
    ui->sZoom->setRange(1,100);
    ui->sPosition->setRange(0,100);
    ui->sPosition->setInvertedControls(true);
    ui->sPosition->setInvertedAppearance(true);

    this->qgliLineX = new QGraphicsLineItem(this->chart);
    this->qgliLineY = new QGraphicsLineItem(this->chart);
    this->qgliLineCoorX = new QGraphicsLineItem(this->chart);
    this->qgtiCoorX = new QGraphicsTextItem(this->chart);
    this->qgtiCoorY = new QGraphicsTextItem(this->chart);
    this->qgtiTextCoorX = new QGraphicsTextItem(this->chart);


    for(igi = 0; igi < Nb_Signal; igi++)
    {
        this->qgliLineCoorY[igi] = new QGraphicsLineItem(this->chart);
        this->qgtiTextCoorY[igi] = new QGraphicsTextItem(this->chart);
    }

    this->qcvChart->setChart(this->chart);
    this->qcvChart->setContentsMargins(0, 0, 0, 0);
    this->qcvChart->setRubberBand(QChartView::NoRubberBand);

    ui->glChart->addWidget(this->qcvChart, 0, 0, Qt::AlignLeft|Qt::AlignBottom);
    ui->glChart->setSpacing(0);
    ui->glChart->setMargin(0);
    ui->glChart->setContentsMargins(0, 0, 0, 0);

    //Apparence
    ui->sZoom->setSliderPosition(ui->sZoom->minimum());
    ui->lZoomValue->setText(QString::number(this->scaledZoom));
    ui->sPosition->setSliderPosition(int(ui->sPosition->maximum()*0.2));
    ui->lPositionValue->setText(QString::number(this->positionZoom));
    ui->lCursorState->setPalette(QPalette(QPalette::Background, QColor(211, 38, 38, 255)));
    ui->lCursorState->setAutoFillBackground(true);
    ui->cbSignType->addItems(SignalGene::getSignaList());

    QStringList qlSignNumber;
    for(igi = 0; igi < Nb_Signal; igi++)
    {
        this->sign[igi] = ui->cbSignType->itemText(0);
        qlSignNumber.append(QString::number(igi+1));
    }
    this->signType = ui->cbSignType->itemText(0);
    this->signNumber = 0;
    ui->cbSignNumber->addItems(qlSignNumber);

    QLineSeries* qlineserie = static_cast<QLineSeries*>(this->chart->series().first());
    this->qgliLineX->setPen(QPen(QBrush(qlineserie->color(), Qt::SolidPattern),
                                 (qreal)1, Qt::DashLine,
                                 Qt::SquareCap, Qt::MiterJoin));
    this->qgliLineY->setPen(QPen(QBrush(qlineserie->color(), Qt::SolidPattern),
                                 (qreal)1, Qt::DashLine,
                                 Qt::SquareCap, Qt::MiterJoin));
    this->qgliLineCoorX->setPen(QPen(QBrush(qlineserie->color(), Qt::SolidPattern),
                                 (qreal)1, Qt::DashLine,
                                 Qt::SquareCap, Qt::MiterJoin));

    this->qgtiCoorX->setDefaultTextColor(QColor(255, 255, 255, 255));
    this->qgtiCoorY->setDefaultTextColor(QColor(255, 255, 255, 255));
    this->qgtiTextCoorX->setDefaultTextColor(QColor(255, 255, 255, 255));

    QFont qgtiFont = this->qgtiCoorX->font();
    qgtiFont.setPixelSize(10);
    this->qgtiTextCoorX->setFont(qgtiFont);

    for(igi = 0; igi < Nb_Signal; igi++)
    {
        qlineserie = static_cast<QLineSeries*>(this->chart->series().at(igi));
        this->qgliLineCoorY[igi]->setPen(QPen(QBrush(qlineserie->color(), Qt::SolidPattern),
                                              (qreal)1, Qt::DashLine,
                                              Qt::SquareCap, Qt::MiterJoin));
        this->qgtiTextCoorY[igi]->setDefaultTextColor(QColor(255, 255, 255, 255));
        this->qgtiTextCoorY[igi]->setFont(qgtiFont);
    }

    this->qcvChart->setRenderHint(QPainter::Antialiasing);
    this->qcvChart->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->qcvChart->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    //Connexion
    QObject::connect(this,
                     &MainWindow::rubberBandChanged,
                     this,
                     &MainWindow::rubberZoomAdapt);

    QObject::connect(ui->bPrint,
                     &QPushButton::clicked,
                     this,
                     &MainWindow::print);

    if(this->chart->series().isEmpty() == false)
    {
        QObject::connect(qobject_cast<QLineSeries*>(this->chart->series().first()),
                         &QLineSeries::hovered,
                         this,
                         &MainWindow::infoLine);
        this->qcvChart->setMouseTracking(true);
    }
    else
        qDebug() << "No Connection between the list and infoLine";

    QObject::connect(this,
                     &MainWindow::infoCursorAsked,
                     this,
                     &MainWindow::infoCursor);

    this->td1->set_callBack_newData(std::bind(&MainWindow::callBack_newData, this));
//    this->qcvChart->installEventFilter(this);
    this->qcvChart->viewport()->installEventFilter(this);   //To grap mouseRelease and mouseMove envent
}

MainWindow::~MainWindow()
{
    delete this->ui;
}

void MainWindow::on_bStart_clicked()
{
    this->td1->startData();
}

void MainWindow::on_bStop_clicked()
{
    this->td1->stopData();
}

void MainWindow::on_dVitesse_valueChanged(int value)
{
    this->td1->setSpeed(value);
}

void MainWindow::on_sZoom_valueChanged(int value)
{
    this->scaledZoom = (float)value/100.0;
    this->scaledZoom = (qLn(this->scaledZoom/0.01)/qLn(10.0))/2;
    this->chart->setZoom(this->scaledZoom);
    ui->lZoomValue->setText(QString("%1").arg(this->scaledZoom, 2, 'f', 2));
}

void MainWindow::on_sPosition_valueChanged(int value)
{
    this->positionZoom = (float)value/100.0;
    this->chart->setPosition(this->positionZoom);
    ui->lPositionValue->setText(QString::number(this->positionZoom));
}

void MainWindow::on_bResetZoom_clicked()
{
    this->chart->resetZoom();
    ui->sZoom->setSliderPosition(ui->sZoom->minimum());
    ui->sPosition->setSliderPosition(int(ui->sPosition->maximum()*0.2));
}

void MainWindow::on_bCursor_clicked()
{
    if(this->bCursorState == false)
    {
        this->bCursorState = true;
        ui->bCursor->setText("Cursor Disable");
        ui->lCursorState->setPalette(QPalette(QPalette::Background, QColor(11, 180, 0, 255)));
        this->qcvChart->setRubberBand(QChartView::HorizontalRubberBand);
        this->qRubberBand = this->qcvChart->findChild<QRubberBand*>();
        this->qRubberBand->installEventFilter(this);
    }
    else
    {
        this->bCursorState = false;
        ui->bCursor->setText("Cursor Enabled");
        ui->lCursorState->setPalette(QPalette(QPalette::Background, QColor(211, 38, 38, 255)));
        this->qcvChart->setRubberBand(QChartView::NoRubberBand);
    }
}

void MainWindow::on_cbSignType_currentTextChanged(const QString &arg1)
{
    this->signType = arg1;
}

void MainWindow::on_cbSignNumber_currentTextChanged(const QString &arg1)
{
    this->signNumber = arg1.toInt()-1;
}

void MainWindow::on_bSignSet_clicked()
{
    this->sign[this->signNumber] = this->signType;
}

void MainWindow::on_bDataBase_clicked()
{
    this->qwDataBase = new QWidget(this, Qt::Window);
    QGridLayout* qglLayoutDataBase = new QGridLayout();
    this->qpteComDataBase = new QPlainTextEdit(this->qwDataBase);

    qglLayoutDataBase->addWidget(this->qpteComDataBase);
    this->qwDataBase->setLayout(qglLayoutDataBase);

    QObject::connect(this->qpteComDataBase,
                     &QPlainTextEdit::textChanged,
                     this,
                     &MainWindow::comDataBaseHandle);

    this->qwDataBase->show();
}

void MainWindow::comDataBaseHandle()
{
    QList<QString> comSQL;
    QList<QString> answer;
    int iCom = 0;
    QString text = this->qpteComDataBase->toPlainText();

    this->qpteComDataBase->setStyleSheet("color:rgba(55, 139, 204, 100%)");

    if((text.length() > 1) && (text.right(2) == ";\n"))
    {
//        if(text.right(2) == ";\n")
//        {
            comSQL.append(text);
            comSQL = comSQL.at(0).split("\n");
            iCom = comSQL.length()-2;
            answer = DataBase::sendCommand(comSQL.at(iCom));

            int i = 0;
            for(i = 0; i < answer.length(); i++)
                this->qpteComDataBase->appendHtml(QString("<font color=#0BB400>")
                                                            + answer.at(i) + "</font>");
            if(i == 0)
                this->qpteComDataBase->appendHtml(QString("<font color=red>Bad command</font>"));

            this->qpteComDataBase->appendPlainText("");
            this->qpteComDataBase->appendPlainText("");
//        }
    }
}

void MainWindow::rubberZoomAdapt(QEvent* event)//, QRubberBand* rb, Chart* ch)
{
    //Q_ASSERT()
//    assert(chart && "Chart ne peut pas être nul!!");
//    assert(qRubberBand != nullptr && "Rubber band ne peut pas ne pas être initialisé");
    //assert(this->children().count > 7);

    static QPointF fp;
    static QPointF tp;
    if(event->type() == QEvent::Resize)
    {
        fp = this->chart->mapToValue(this->qRubberBand->geometry().topLeft());
        tp = this->chart->mapToValue(this->qRubberBand->geometry().bottomRight());
    }
    if(event->type() == QEvent::Hide)   //correspond to the mouse release event when the zone selected is validate
    {
        this->chart->zoomInRubberBand(fp.x(), tp.x());
    }
}

void MainWindow::print(bool b)
{
    QPrinter printer(QPrinter::HighResolution);
    QPrintDialog printDialog(&printer);
    QObject::connect(&printDialog, &QDialog::accepted, [&]
    {//Function that is called when accepted signal is emited
        printer.setOrientation(QPrinter::Portrait);//QPrinter::Landscape
//        printer.setPageSize(QPageSize(QPageSize::A4));
        printer.setPaperSize(QPrinter::A4);
        printer.setResolution(1200);//dpi
        printer.setFullPage(true);
        printer.setPageMargins(QMarginsF(0,0,0,0), QPageLayout::Millimeter);

        auto source = this->qcvChart->viewport()->rect();
        auto pageMM = printer.pageRect(QPrinter::Millimeter);
        auto pageDP = printer.pageRect(QPrinter::DevicePixel);
        auto scaleDPH = (qreal)(pageDP.height()/source.height());
        auto scaleDPW = (qreal)(pageDP.width()/source.width());
        auto scaleMMH = (qreal)(pageMM.height()/source.height());
        auto scaleMMW = (qreal)(pageMM.width()/source.width());
        auto targetDP = QRectF(pageDP.topLeft(), QSizeF(source.width(), source.height()*scaleDPH));
        auto targetMM = QRectF(pageMM.topLeft(), QSizeF(source.width()*scaleMMH, source.height()*scaleMMH));
        auto target = targetDP;
        qDebug() << "(print): source: " << source
                 << "; pageDP: " << pageDP
                 << "; pageMM: " << pageMM
                 << "; scaleDPH: " << scaleDPH
                 << "; scaleDPW: " << scaleDPW
                 << "; scaleMMH: " << scaleMMH
                 << "; scaleMMW: " << scaleMMW
                 << "; targetDP: " << targetDP
                 << "; targetMM: " << targetMM;

        QPainter painter(&printer);
        this->qcvChart->render(&painter, pageDP, source, Qt::KeepAspectRatioByExpanding);
    });
    printDialog.show(); // modal on OS X thus must follow `connect` above
}

void MainWindow::callBack_newData()
{
    int i = 0;
    float val[Nb_Signal];
    float dbMesure[8];

    dbMesure[0] = (int)SignalGene::getIndexData();
    for(i = 0; i < Nb_Signal ; i++)
    {
        SignalGene::setSignal(this->sign[i]);
        val[i] = SignalGene::getNumber(i);
        dbMesure[i+1] = val[i];
    }
    this->chart->addValue(val);

    dbMesure[6] = 1;
    dbMesure[7] = 1;
#if DATABASE == 1
    DataBase::addMesure(dbMesure);
#endif
}

void MainWindow::infoLine(QPointF point, bool state)
{
    if(state == true)
    {
//        qDebug() << "(infoLine): hovered: QChartView: " << this->chart->mapToPosition(point);
        this->qgliLineX->setLine(this->chart->mapToPosition(point).x(),
                                 this->chart->mapToPosition(point).y(),
                                 this->chart->mapToPosition(point).x(),
                                 this->chart->mapToPosition(QPointF(point.x(), (qreal)this->chart->getMinAxisY())).y());
        this->qgliLineY->setLine(this->chart->mapToPosition(point).x(),
                                 this->chart->mapToPosition(point).y(),
                                 this->chart->mapToPosition(QPointF((qreal)this->chart->getMinAxisX(), point.y())).x(),
                                 this->chart->mapToPosition(point).y());

        this->qgtiCoorX->setHtml(QString("<div style='background:rgba(11, 180, 0, 100%);'>")
                                 + QString("%1").arg(point.x(), 2, 'f', 2) + "</div>");
        this->qgtiCoorY->setHtml(QString("<div style='background:rgba(11, 180, 0, 100%);'>")
                                 + QString("%1").arg(point.y(), 2, 'f', 2) + "</div>");
        QPointF pCoorX = this->chart->mapToPosition(QPointF(point.x(), (qreal)this->chart->getMinAxisY()));
        pCoorX.setX(pCoorX.x()-(this->qgtiCoorX->boundingRect().width()/2));
        this->qgtiCoorX->setPos(pCoorX);
        QPointF pCoorY = this->chart->mapToPosition(QPointF((qreal)this->chart->getMinAxisX(), point.y()));
        pCoorY.setX(pCoorY.x()-this->qgtiCoorY->boundingRect().width());
        pCoorY.setY(pCoorY.y()-(this->qgtiCoorY->boundingRect().height()/2));
        this->qgtiCoorY->setPos(pCoorY);

        this->qgtiCoorX->update();
        this->qgtiCoorX->show();
        this->qgtiCoorY->update();
        this->qgtiCoorY->show();
        this->qgliLineX->update();
        this->qgliLineX->show();
        this->qgliLineY->update();
        this->qgliLineY->show();
    }
    else
    {
        this->qgtiCoorX->hide();
        this->qgtiCoorY->hide();
        this->qgliLineX->hide();
        this->qgliLineY->hide();
    }
}

void MainWindow::infoCursor(QEvent* event)
{
    static bool pressed = false;
//    QMouseEvent* me = qobject_cast<QMouseEvent*>(event);  //Doen't work and don't know why
//    const QMouseEvent* const me = static_cast<const QMouseEvent*>(event); //Best way to cast ?
    QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event); //Works well but don't know if it the best way to cast
    static QMouseEvent oldMouseEvent = *mouseEvent;

    if(mouseEvent->type() == QMouseEvent::MouseButtonPress)
    {
        this->updateCursor(true, mouseEvent);
        oldMouseEvent = *mouseEvent;
        pressed = true;
    }
    if(mouseEvent->type() == QMouseEvent::MouseButtonRelease)
    {
        this->updateCursor(false, mouseEvent);
        pressed = false;
    }
    if(mouseEvent->type() == QMouseEvent::MouseMove)
        if(pressed == true)
        {
            this->updateCursor(true, mouseEvent);
            oldMouseEvent = *mouseEvent;
        }

    if(event->type() == QEvent::Paint)
        if(pressed == true)
            this->updateCursor(true, &oldMouseEvent);
}

void MainWindow::updateCursor(bool visible, QMouseEvent* mouseEvent)
{
    int iSerie = 0;
    for(iSerie = 0; iSerie < Nb_Signal; iSerie++)
    {
        int iPf = 0;
        QPointF pf(0,0);
        QLineSeries* qlineserie = static_cast<QLineSeries*>(this->chart->series().at(iSerie));
        QList<QPointF> qlistSerie = qlineserie->points();

        if(qlistSerie.isEmpty() == false)
        {
            if(visible == true)
            {
                iPf = (int)(this->chart->mapToValue(mouseEvent->pos()).x());
                if(iPf <= 0)
                    pf = qlistSerie.first();
                else if(iPf >= qlistSerie.length())
                    pf = qlistSerie.last();
                else
                    pf = qlistSerie.at(iPf);

                if(iSerie == 0)
                {
                    this->qgliLineCoorX->setLine(this->chart->mapToPosition(pf).x(),
                                                 this->chart->mapToPosition(QPointF((qreal)0, (qreal)this->chart->getMinAxisY())).y(),
                                                 this->chart->mapToPosition(pf).x(),
                                                 this->chart->mapToPosition(QPointF((qreal)0, (qreal)this->chart->getMaxAxisY())).y());
                    this->qgtiTextCoorX->setHtml(QString("<div style='background:rgba(")
                                                         + QString("%1, %2, %3, %4").arg(qlineserie->color().red()).
                                                                                     arg(qlineserie->color().green()).
                                                                                     arg(qlineserie->color().blue()).
                                                                                     arg(qlineserie->color().alpha())
                                                         + ");'>"
                                                         + QString("%1").arg(pf.x(), 2, 'f', 2) + "</div>");
                    this->qgtiTextCoorX->setZValue(this->chart->zValue()+3);
                    QPointF pCoorX = this->chart->mapToPosition(QPointF(pf.x(), (qreal)this->chart->getMinAxisY()));
                    pCoorX.setX(pCoorX.x()-(this->qgtiTextCoorX->boundingRect().width()/2));
                    this->qgtiTextCoorX->setPos(pCoorX);
                    this->qgliLineCoorX->update();
                    this->qgliLineCoorX->show();
                    this->qgtiTextCoorX->update();
                    this->qgtiTextCoorX->show();
                }
                this->qgliLineCoorY[iSerie]->setLine(this->chart->mapToPosition(QPointF((qreal)this->chart->getMinAxisX(), (qreal)0)).x(),
                                                     this->chart->mapToPosition(pf).y(),
                                                     this->chart->mapToPosition(QPointF((qreal)this->chart->getMaxAxisX(), (qreal)0)).x(),
                                                     this->chart->mapToPosition(pf).y());
                this->qgtiTextCoorY[iSerie]->setHtml(QString("<div style='background:rgba(")
                                                             + QString("%1, %2, %3, %4").arg(qlineserie->color().red()).
                                                                                          arg(qlineserie->color().green()).
                                                                                          arg(qlineserie->color().blue()).
                                                                                          arg(qlineserie->color().alpha())
                                                             + ");'>"
                                                             + QString("%1").arg(pf.y(), 2, 'f', 2) + "</div>");
                this->qgtiTextCoorY[iSerie]->setZValue(this->chart->zValue()+3);
                QPointF pCoorY = this->chart->mapToPosition(QPointF((qreal)this->chart->getMinAxisX(), pf.y()));
                pCoorY.setX(pCoorY.x()-this->qgtiTextCoorY[iSerie]->boundingRect().width());
                pCoorY.setY(pCoorY.y()-(this->qgtiTextCoorY[iSerie]->boundingRect().height()/2));
                this->qgtiTextCoorY[iSerie]->setPos(pCoorY);
                this->qgliLineCoorY[iSerie]->update();
                this->qgliLineCoorY[iSerie]->show();
                this->qgtiTextCoorY[iSerie]->update();
                this->qgtiTextCoorY[iSerie]->show();

            }
            else
            {
                if(iSerie == 0)
                {
                    this->qgliLineCoorX->hide();
                    this->qgtiTextCoorX->hide();
                }
                this->qgliLineCoorY[iSerie]->hide();
                this->qgtiTextCoorY[iSerie]->hide();
            }
        }
    }
}

void MainWindow::resizeEvent(QResizeEvent* event)
{
    QMainWindow::resizeEvent(event);
    this->qcvChart->resize(ui->glChart->cellRect(0,0).size());
    this->chart->resize(this->qcvChart->size());
    this->chart->setZoom(ui->sZoom->sliderPosition()/100);
}

void MainWindow::showEvent(QShowEvent* event)
{
    QMainWindow::showEvent(event);
    this->qcvChart->resize(ui->glChart->cellRect(0,0).size());
    this->chart->resize(this->qcvChart->size());
}

bool MainWindow::eventFilter(QObject *watched, QEvent *event)
{
    if(watched == this->qRubberBand)
        emit this->rubberBandChanged(event);

//    if(watched == this->qcvChart) //Doesn't work for mouseRelease and mouseMove envent
//        emit this->infoCursorAsked(event);
    if(watched == this->qcvChart->viewport())
        emit this->infoCursorAsked(event);

    return QMainWindow::eventFilter(watched, event);
}

void MainWindow::closeEvent (QCloseEvent *event)
{
//    if(this->qwDataBase)
//    {
//        this->qwDataBase->close();
//        delete (this->qwDataBase);
//        this->qwDataBase = nullptr;
//    }
    this->td1->endTickData();
}

void MainWindow::on_bVersion_clicked()
{
    this->client->getVersion();
}
